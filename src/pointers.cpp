#include <iostream>

using namespace std;


int main()
{
  int x = 25, y = 2 * x;    
  auto a = &x, b = a;
  auto c = *a, d = 2 * *b;
  
  // x = int
  // y = int
  // pointer a is x address 
  // pointer b is same as x address


  cout<<"x value = "<< x <<" and address = "<< &x <<endl; 
  cout<<"y value = "<< x <<" and address = "<< &y <<endl; 
  cout<<"a value = "<< *a <<" and address = "<< a <<endl; 
  cout<<"b value = "<< *b <<" and address = "<< b <<endl; 
  cout<<"c value = "<< c <<" and address = "<< &c <<endl; 
  cout<<"d value = "<< d <<" and address = "<< &d <<endl; 



}
